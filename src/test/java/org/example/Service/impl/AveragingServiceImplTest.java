package org.example.Service.impl;

import org.example.Model.Villagers;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AveragingServiceImplTest {

    @Test
    void parameterToTest() {
        var averagingService = new AveragingServiceImpl();
        List<Integer> ageOfDeath = Arrays.asList(10, 13);
        List<Integer> yearOfDeath = Arrays.asList(12, 17);
        Villagers villagers = new Villagers(2, ageOfDeath, yearOfDeath);
        assertEquals(4.5, averagingService.countBirth(villagers));
    }
}