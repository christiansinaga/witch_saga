package org.example;

import org.example.Service.AveragingService;
import org.example.Service.impl.AveragingServiceImpl;

public class Main {

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) {
        AveragingService averagingService = new AveragingServiceImpl();
        while (true) {
            double average = averagingService.countAverage();
            if (average > 0) System.out.printf("The Average is : " + average + "\r\n\n");
            else System.out.printf("Invalid data : " + average + "\r\n\n");
        }
    }
}