package org.example.Model;

import java.util.List;

public class Villagers {
    private Integer peopleKnown;
    private List<Integer> ageOfDeath;
    private List<Integer> yearOfDeath;

    public Villagers(Integer peopleKnown) {
        this.peopleKnown = peopleKnown;
    }

    public Villagers(Integer peopleKnown, List<Integer> ageOfDeath, List<Integer> yearOfDeath) {
        this.peopleKnown = peopleKnown;
        this.ageOfDeath = ageOfDeath;
        this.yearOfDeath = yearOfDeath;
    }

    public Integer getPeopleKnown() {
        return peopleKnown;
    }

    public void setPeopleKnown(Integer peopleKnown) {
        this.peopleKnown = peopleKnown;
    }

    public List<Integer> getAgeOfDeath() {
        return ageOfDeath;
    }

    public void setAgeOfDeath(List<Integer> ageOfDeath) {
        this.ageOfDeath = ageOfDeath;
    }

    public List<Integer> getYearOfDeath() {
        return yearOfDeath;
    }

    public void setYearOfDeath(List<Integer> yearOfDeath) {
        this.yearOfDeath = yearOfDeath;
    }

    @Override
    public String toString() {
        return "Villagers{" +
                "peopleKnown=" + peopleKnown +
                ", ageOfDeath=" + ageOfDeath +
                ", yearOfDeath=" + yearOfDeath +
                '}';
    }
}
