package org.example.Model;

import java.util.List;

public class TheVillage {
    private List<Integer> birthOfYear;
    private List<Integer> peopleKilledOnYear;

    public TheVillage(List<Integer> birthOfYear) {
        this.birthOfYear = birthOfYear;
    }

    public TheVillage(List<Integer> birthOfYear, List<Integer> peopleKilledOnYear) {
        this.birthOfYear = birthOfYear;
        this.peopleKilledOnYear = peopleKilledOnYear;
    }

    public List<Integer> getBirthOfYear() {
        return birthOfYear;
    }

    public void setBirthOfYear(List<Integer> birthOfYear) {
        this.birthOfYear = birthOfYear;
    }

    public List<Integer> getPeopleKilledOnYear() {
        return peopleKilledOnYear;
    }

    public void setPeopleKilledOnYear(List<Integer> peopleKilledOnYear) {
        this.peopleKilledOnYear = peopleKilledOnYear;
    }

    @Override
    public String toString() {
        return "Village{" +
                "birthOfYear=" + birthOfYear +
                ", peopleKilledOnYear=" + peopleKilledOnYear +
                '}';
    }
}
