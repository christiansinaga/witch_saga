package org.example.Service.impl;

import org.example.Model.TheVillage;
import org.example.Model.Villagers;
import org.example.Service.AveragingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class AveragingServiceImpl implements AveragingService {

    protected static final String Question = " will death ?";

    @Override
    public Double countAverage() {
        System.out.println("How many people are known ?");
        Scanner intScanner = new Scanner(System.in);
        int Input = intScanner.nextInt();

        List<Integer> ageInput = new ArrayList<>();
        List<Integer> yearInput = new ArrayList<>();
        for (int count = 1; count <= Input; count++) {
            System.out.println("What Age person " + count + Question);
            Scanner ageScanner = new Scanner(System.in);
            ageInput.add(ageScanner.nextInt());

            System.out.println("What Year person " + count + Question);
            Scanner yearScanner = new Scanner(System.in);
            yearInput.add(yearScanner.nextInt());
        }
        Villagers villagers = new Villagers(Input, ageInput, yearInput);

        return (countBirth(villagers));
    }

    public Double countBirth(Villagers villagers) {
        List<Integer> listBirth = new ArrayList<>();
        for(int count = 0; count < villagers.getPeopleKnown(); count++) {
            int birthYear = villagers.getYearOfDeath().get(count) - villagers.getAgeOfDeath().get(count);
            if (birthYear < 0) return -1.0;
            listBirth.add(birthYear);
        }
        return countPeopleKilled(new TheVillage(listBirth));
    }

    private Double countPeopleKilled(TheVillage theVillage) {
        List<Integer> listKilled = new ArrayList<>();
        theVillage.getBirthOfYear().forEach(theYear -> {
            int peopleKilled = 1;
            for (int times = 0; times < theYear; times++) {
                peopleKilled = peopleKilled + times;
            }
            listKilled.add(peopleKilled);
        });
        theVillage.setPeopleKilledOnYear(listKilled);

        int count = 0;
        for (Integer integer : listKilled) count = count + integer;

        return (double) count / theVillage.getPeopleKilledOnYear().size();
    }
}
